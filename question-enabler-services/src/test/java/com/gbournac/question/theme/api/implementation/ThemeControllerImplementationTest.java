package com.gbournac.question.theme.api.implementation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.gbournac.question.theme.domain.ThemeController;

class ThemeControllerImplementationTest {

	ThemeController controller;

	@BeforeEach
	void setUp() throws Exception {
		this.controller = new ThemeControllerImplementation();
	}

	@Test
	void test() {
		assertEquals("Home", controller.getHome());
	}
}
